
module.exports.task1 = async function firstTask (param={}){
    const {file,size,digit,generate}=param;
    const fs = require('fs')
    const path = require('path')
    const rankPerItem=[]
    const data=[]
  
   

    class RangingAlgo{

        static ids=[
            {start:'q',end:'B',digits:"3567319683"},
            {start:'C',end:'C',digits:"3542170949"},
            {start:'b',end:'t',digits:"3546696871"},
            {start:'a',end:'t',digits:"0963437794"},
            {start:'a',end:'t',digits:"0542137945"},
            {start:'a',end:'z',digits:"4542137945"},
            {start:'a',end:'b',digits:"9042137948"},
            {start:'z',end:'b',digits:"9042137945"},
            {start:'z',end:'z',digits:"9042107945"},
            {start:'b',end:'z',digits:"9042147945"},
            {start:'x',end:'z',digits:"9042147940"},
            {start:'R',end:'x',digits:"9042147945"},
            {start:'f',end:'z',digits:"9042147935"},
            {start:'o',end:'z',digits:"9042117945"},
            {start:'y',end:'e',digits:"0002147945"},
            {start:'z',end:'f',digits:"0022147945"},
            {start:'b',end:'c',digits:"9002147945"},
            {start:'b',end:'c',digits:"9002147948"},
            {start:'b',end:'c',digits:"9002147978"},
            {start:'b',end:'c',digits:"4012147948"},


            
            ]
       
        static loadData(file='identifications.txt',size=null){
         let data = fs.readFileSync(path.join(__dirname,file),'utf-8')
          data=data.split('\n')
          data = data.map(item=>({start:item.charAt(0),end:item.charAt(item.length-1),digits:item.split(item.charAt(0)).join('').split(item.charAt(item.length-1)).join('')}))
          data=data.filter(item=>item.start!='') 
          if(size){ 
            RangingAlgo.ids=data.filter((item,index)=>index<=size)
          }else{
            RangingAlgo.ids=data
          }
        }
     
        static generatePopulation(length=2000,digitLength=8,file='test.txt'){
            const letters='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            for( let index=0;index<length;index++){
                var digist=[];

                    digist = (()=>{
                        const d=[]
                        for(let i=0;i<digitLength;i++){
                            d.push(Math.floor(Math.random()*(digitLength-1)))
                        }
                        return d
                    })()
            
                const start = letters.charAt(Math.floor(Math.random()*(letters.length-1)))
                const end =letters.charAt(Math.floor(Math.random()*(letters.length-1)))
                const id= start+digist.join('')+end
                fs.writeFileSync(file,id+"\n",{flag:'a+'})

            }
        
        }
         
        
        /**
         * 
         * @param {*} list 
         * @returns list
         */
    
        static filter(list=[]){
           return list.filter((item,index)=> index<list.length? item!=list[index+1]:null)
        } 
    
    
       static toLowercase(listOfString=[]){
           return  listOfString.flatMap(item=>item.toLowerCase())
        }
    
        /**
         * Retunr a list of orders alphabet letters
         * @param {*} StringList 
         * @returns 
         */
    
       static Order(StringList=[]){
        return [...StringList].sort()
        }
    
     static rankByLetter(list=[],letter){
        return  list.indexOf(letter) + 1
     }
        
    
    }

    
function fitness(attr="startRank",dt=data,target){
    var score=0;
    var ids=[]
     for( let i=0;i<sortedList.length;i++){
       if((sortedList[i][attr]- Math.min(...dt))==0 && target!=i){
           score= sortedList[target][attr];
           ids.push({rank:sortedList[i][attr],pk:i})
       }
    }
    return {score,ids,target}
} 
const endRanks=[]

if(generate && size){
    RangingAlgo.generatePopulation(size,digit,file)
}
if(size){
    RangingAlgo.loadData(file,size)
}else{
   var sz=1000
   try{
    console.log(`Warning: you have not provied a size of the dataset to be used! \n we are testing over at least ${sz} lines of the provided file. \n Please wait until we print the results.`)
   
  
     RangingAlgo.loadData(file,sz)

 
   }catch(err){
    throw new Error(err.message)
   }
 

}

// List of strings
const startStrings = RangingAlgo.toLowercase(RangingAlgo.ids.map((item)=>item.start));
const endStrings = RangingAlgo.toLowercase(RangingAlgo.ids.map(item=>item.end)) 
 

var startOrders=RangingAlgo.Order(startStrings) 
var endOrders = RangingAlgo.Order(endStrings)
startOrders=RangingAlgo.filter(startOrders)
endOrders= RangingAlgo.filter(endOrders)
const sortedList=RangingAlgo.ids
const digits=[]
    RangingAlgo.ids.forEach(item=>{
        const startorder= RangingAlgo.rankByLetter(startOrders,item.start.toLowerCase())
        const endorder =RangingAlgo.rankByLetter(endOrders,item.end.toLowerCase())
        item.endRank=endorder
        item.startRank=startorder
        const digist=[]
        for( let i=0;i<item.digits.length;i++){
            digist.push(parseInt(item.digits.charAt(i)))
            digits.push(parseInt(item.digits.charAt(i)))
        }
       item.digits= digist
    })
    
    var orderedDigists=RangingAlgo.Order(digits)
    orderedDigists=RangingAlgo.filter(orderedDigists)
    sortedList.forEach((item,index)=>{
    const digistRanking=[]
    item.digits.forEach(item=>{
        digistRanking.push({
            digist:item,
            rank:RangingAlgo.rankByLetter(orderedDigists,item)
        })
    })
  item.digitsRanking=digistRanking
   data.push(item.startRank)
   endRanks.push(item.endRank)
})
sortedList.forEach((item,index)=>{
    const {score:startScore}=fitness('startRank',data,index)
    const {score:endScore}=fitness('endRank',endRanks,index)
    rankPerItem.push({id:index,endScore,startScore,digits:item.digitsRanking,code:item.start+item.digits.join('')+item.end})
}) 
return rankPerItem

}
  
module.exports.task2= async function secondTask(param={}){
    const {digit,data}=param
    const rankedItems=[];
    function numFitness(list=[],index=0,start=0){
        const digits=[];
        var score=0;
        const currentDigits=list[index].digits;
        for(let i=0;i<list.length;i++){
            if(i!=index){
                digits.push(list[i]?.digits);
            }
        }

    
       for(let d=0;d<digits.length;d++){
        if(digits[d][start].rank>currentDigits[start].rank){
            score++
         } 
       }
       
    
    return {score,counts:digits.length}
    
    }
    
    function  rankingByDigits(firstItems=[],items=[],start=0){
        for(let index=0;index<firstItems.length;index++){
            let {score,counts}=numFitness(firstItems,index,start) 
            if(score==counts){
                if(rankedItems.length==0){
                rankedItems.unshift(firstItems[index].id)
                }else{
                rankedItems.push(firstItems[index].id)
                }
                items.splice(items.indexOf(firstItems[index]),1)
                firstItems.splice(index,1)
            }
          }
          //console.log(start)
        return {firstItems,items} 
    } 

    function ranking(firstItems,digitsLenth=8){
        let items=firstItems
           if(firstItems.length>1){
            const starts=firstItems.map(item=>item.startScore);
            firstItems=firstItems.filter(item=>item.startScore==Math.min(...starts));  
            if(firstItems.length==1){
               if(rankedItems.length==0){
                   rankedItems.unshift(firstItems[0].id)
               }else{
                   rankedItems.push(firstItems[0].id)
               }
             items.splice(items.indexOf(firstItems[0]),1);
            }else{
           const ends=firstItems.map(item=>item.endScore);
           firstItems=firstItems.filter((item)=>item.endScore==Math.min(...ends));
           if(firstItems.length==1){
             
               if(rankedItems.length==0){
                   rankedItems.unshift(firstItems[0].id);
               }else{
                   rankedItems.push(firstItems[0].id);
               }
               items.splice(items.indexOf(firstItems[0]),1);

           }else{
           let rs={firstItems,items};  
           for(let i=0;i<digitsLenth;i++ && firstItems.length){
                   rs=rankingByDigits(firstItems,items,i)
                   items=rs.items
                   firstItems=rs.firstItems
                   if(firstItems.length==1){
                    if(rankedItems.length){
                        rankedItems.push(firstItems[0].id);
                    }else{
                        rankedItems.unshift(firstItems[0].id);
                    }
                    items.splice(items.indexOf(firstItems[0]),1); 
                    firstItems.splice(0,1) 
                    break
                   }
           }       
           }  
           }
        }
           else{
           if(firstItems.length==1){
            if(rankedItems.length){
                rankedItems.push(firstItems[0].id);
            }else{
                rankedItems.unshift(firstItems[0].id);
            }
            items.splice(items.indexOf(firstItems[0]),1); 
            firstItems.splice(0,1) 
           }  
              
           } 
    return items
         
    }
 
    const dt=data.sort()
    let remains=ranking(dt,digit)||[]; 
    while (remains.length>0) {
        remains=ranking(remains,digit);  
    }  
    return rankedItems;
 
} 
 

 





     



