# Amineware Recruitment Test - Employees Identification Sorting

## Overview

This repository contains my solution to the Amineware Recruitment Test for solving the Employees Identification Sorting problem.

### Installation

```bash
# Clone the repository
git clone https://gitlab.com/DAVE800/amineware.test.recrutment.tn.git

# Navigate to the project directory
cd amineware.test.recrutment.tn

# Install dependencies
npm install
# runing instructions: s is the size of the dataset to be used; f is the file to be read ; 
# d is the number of digit within the employee identification ; o is the output file; g is use to request dataset spawing by the system  given the parameters above.

npm start s=200 g=true f=filename.txt d=8 o=output

# in case you want to enter a custum file you just have to create the file in the directory of the project with each item of the content separeted by \n 
# in that case you can run the script with the following command:
npm start f=nameofyourfile.txt s=200 g=8 g=false o=output.xt
