

const { DynamicPool } = require('node-worker-threads-pool');
const { argv } = require('process');
const { task1,task2 } = require('./tasks');
const path=require('path')
const fs = require('fs')
const worker = require('node:worker_threads');
const {
    performance,
    PerformanceObserver,
  } = require('node:perf_hooks');
  const  momentDurationFormatSetup =require('moment-duration-format')
 const moment= require('moment')
 const numCPUs = require('os').cpus().length;

 
 momentDurationFormatSetup(moment);

const dynamicPool = new DynamicPool(numCPUs);
let rankedItems=[];

/**
 *  
 * @param {*} file 
 * @param {*} size 
 * @param {*} digit 
 * @param {*} generate 
 * @param {*} output 
 */
async function  exec(file,size=null,digit=8,generate=false,output='output.txt'){
   try{ 
   const param={file:file?file:'identifications.txt',size:size?size:null,digit:digit?digit:8,generate:generate?generate:false}
   const data = await dynamicPool.exec({
       param,
       task:task1
    }) 

    Object.assign(param,{data})

      const ranks=await dynamicPool.exec({
       task:task2,
       param
      })

       ranks.forEach(item=>{
           rankedItems.push(data.filter(d=>d.id==item)[0])
       }) 

       rankedItems=rankedItems.map((item,index)=>({code:item.code,rank:index+1,id:item.id}));
        var lines = rankedItems.map(item=>item.code)
         lines=lines.join('\n') 
         try{
             fs.writeFileSync(output,lines,{flag:'a+'}) 
           }catch(err){
               throw new Error(err.message)
           }  
 
       console.log('**** Start results table. *********')
       console.table(rankedItems)
       console.log('***End results table. ****')
   }catch(err){
    throw new Error(err.message)
   }
 
  
} 


(async()=>{
    try{

        const args=argv.filter((item,index)=>index>1);
         var size= args.filter(item=>item.includes('s='));
         var file = args.filter(item=>item.includes('f='));
         var digist = args.filter(item=>item.includes('d='))
         var generate = args.filter(item=>item.includes('g='))
         var output = args.filter(item=>item.includes('o='))
         size =size.length?parseInt(size.join('').split('=')[1]):null;
         file =file.length?(file.join('')).split('=')[1]:"identifications.txt";
         digist = digist.length?parseInt(digist.join('').split('=')[1]):null;
         generate = generate.length?JSON.parse(generate.join('').split('=')[1]):false;
         output =output.length?(output.join('')).split('=')[1]:((new Date()).toTimeString()).split(' ')[0].split(':').join('')+"-output.txt";
         
         console.log('*** Execution started ***')
         console.log('*** Please wait ***')
         const wrapped = performance.timerify(exec);
 
          const obs = new PerformanceObserver((list) => {
            const {duration}=list.getEntries()[0];
             const time=moment.duration(Math.floor(duration),'milliseconds').format()
             console.log('execution time (min:ss): ',time)
             console.log('*** Execution ended ***')
            performance.clearMarks();
            performance.clearMeasures(); 
            obs.disconnect();
        }); 
        obs.observe({ entryTypes: ['function'] });
        wrapped(file,size,digist,generate,output) 
    }catch(err){
        throw new Error(err.message)
    } 
})()